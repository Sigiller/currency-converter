const { resolve } = require('path');

module.exports = [
    require('./module.rules.pcss'),
    require('./module.rules.img'),
    require('./module.rules.babel'),
    {
        enforce : 'pre',
        test    : /\.jsx?$/,
        exclude : [
            resolve(global.webpack.context, 'public'),
        ],
        use: [{
            loader  : 'eslint-loader',
            options : {
                failOnWarning: global.webpack.production,
                failOnError  : global.webpack.production,
                emitError    : global.webpack.production,
                emitWarning  : global.webpack.production,
                cache: global.webpack.development
            }
        }]
    }, {
        test    : /\.(webapp|ico)$/,
        include : [
            resolve(global.webpack.context, 'app', 'html'),
        ],
        use: [{
            loader  : 'file-loader',
            options : {
                name      : global.webpack.production ? '[hash:hex].[ext]' : '[name].[ext]',
                publicPath: (url) => url.replace('./../../public', ''),
                outputPath: (url) => `./../../public/${url}`,
                emitFile  : false,
            }
        }]
    }, {
        test    : /\.(png|jpe?g|gif|svg|xml|woff|woff2|eot)$/,
        include : [
            resolve(global.webpack.context, 'app', 'html'),
            resolve(global.webpack.context, 'app', 'fonts')
        ],
        use: [{
            loader  : 'file-loader',
            options : {
                name      : '[name].[ext]',
                publicPath: (url) => url.replace('./../../public', ''),
                outputPath: (url) => `./../../public/${url}`,
                emitFile  : false
            }
        }]
    }, {
        test    : /\.html$/,
        include : [
            resolve(global.webpack.context, 'public'),
        ],
        use     : [{
            loader: 'html-loader'
        }]

    }, {
        test: /\.css$/,
        loader: 'style-loader!css-loader',
        include: /flexboxgrid/
    }
];
