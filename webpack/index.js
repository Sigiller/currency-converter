const { resolve } = require('path');

module.exports = (env = {}) => {
    global.webpack = {
        context     : resolve(__dirname, '..'),
        dir         : __dirname,
        env         : process.env.NODE_ENV || (env.production ? 'production' : 'development'),
        config      : process.env.CONFIG,
        development : !env.production,
        production  : !!env.production,
    };

    process.env.NODE_ENV = global.webpack.env;
    process.env.DEBUG = env.debug;

    const config = {
        context         : global.webpack.context,
        entry           : require('./entry'),
        devtool         : require('./devtool'),
        target          : 'web',
        output          : require('./output'),
        module          : {
            rules : require('./module.rules')
        },
        plugins         : require('./plugins'),
        performance     : { hints: global.webpack.development ? false : 'warning' },
        bail            : global.webpack.production,
        profile         : global.webpack.production
    };

    if(global.webpack.development) {
        config.devServer = require(`./devServer`);
    }

    return config;
};
