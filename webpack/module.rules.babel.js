const { resolve } = require('path');

module.exports =  {
    test    : /\.jsx?$/,
    include : [
        resolve(global.webpack.context, 'app'),
    ],
    use: [{
        loader  : 'babel-loader',
        options : {
            forceEnv      : global.webpack.env,
            cacheDirectory: global.webpack.development
        }
    }]
};
