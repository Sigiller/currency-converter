const path                  = require('path');
const webpack               = require('webpack');
const CleanPlugin           = require('clean-webpack-plugin');
const ExtractTextPlugin     = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin     = require('html-webpack-plugin');

const base = [
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en|ru/)
];

const common = [
    new webpack.DefinePlugin({
        __CLIENT__              : true,
        __SERVER__              : false,
        __PRODUCTION__          : global.webpack.production,
        __DEVELOPMENT__         : global.webpack.development,
        'process.env.NODE_ENV'  : JSON.stringify(global.webpack.env),
        'process.env.CONFIG'    : JSON.stringify(global.webpack.config)
    }),
    new ExtractTextPlugin({
        filename    : '[contenthash].css',
        allChunks   : true,
        disable     : global.webpack.development
    }),
    new HtmlWebpackPlugin({
        template    : 'app/index.html'
    })
];

const config = {
    development : [
        ...common,
        ...base
    ],
    production  : [
        ...common,
        ...base,
        new webpack.NamedModulesPlugin(),
        new webpack.NamedChunksPlugin((chunk) => {
            if (chunk.name) {
                return chunk.name;
            }
            return chunk.mapModules(({ context, request }) => path.relative(context, request)).join('_');
        }),
        new CleanPlugin([
            path.resolve(global.webpack.context, 'public')
        ], {
            verbose : global.webpack.development,
            dry     : global.webpack.production
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name    : 'vendors',
            minChunks: Infinity
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'runtime'
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            },
            sourceMap: false,
            comments: (astNode, comment) => false
        })
    ]
};

module.exports = config[global.webpack.env];
