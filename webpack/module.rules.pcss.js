const { resolve } = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const browsers = [
    'Chrome >= 50',
    'Firefox >= 50',
    'Explorer >= 11',
    'Edge >= 12',
    'iOS >= 8',
    'Safari >= 8',
    'ExplorerMobile >= 10',
    'Opera >= 40'
];

const use = [{
    loader : 'style-loader'
}, {
    loader : 'css-loader',
    options: {
        context         : global.webpack.context,
        modules         : true,
        sourceMap       : global.webpack.development,
        minimize        : global.webpack.production,
        localIdentName  : global.webpack.production ? '[hash:hex]' : '[local]_[hash:hex:5]'
    }
}, {
    loader : 'postcss-loader',
    options: {
        plugins: [
            require('doiuse')({
                browsers,
                ignore: [
                    'flexbox',
                    'viewport-units',
                    'pointer-events',
                    'outline'
                ]
            }),
            require('stylelint')({
                configBasedir: global.webpack.context
            }),
            require('postcss-nested'),
            require('autoprefixer')({ browsers })
        ]
    }
}];

if(global.webpack.production) {
    delete use.shift();
}

module.exports = {
    test    : /\.pcss$/,
    use     : ExtractTextPlugin.extract({ use }),
    include : [
        resolve(global.webpack.context, 'app'),
        resolve(global.webpack.context, 'node_modules')
    ]
};
