const { resolve } = require('path');

const result = {
    development : {
        path            : resolve(global.webpack.context, 'public'),
        publicPath      : '/',
        chunkFilename   : '[name].js',
        filename        : '[name].js'
    },
    production  : {
        path            : resolve(global.webpack.context, 'public'),
        filename        : '[name].[chunkhash].js',
        publicPath      : '/'
    }
};

module.exports = result[global.webpack.env];
