const { resolve } = require('path');

module.exports = {
    host            : 'localhost',
    port            : 8080,
    contentBase     : resolve(global.webpack.context, 'public'),
    hot             : true,
    disableHostCheck: true,
    inline          : true,
    lazy            : false,
    quiet           : false,
    noInfo          : false,
    stats           : {
        hash        : false,
        version     : false,
        timings     : true,
        cached      : true,
        errorDetails: true,
        colors      : true
    }
};
