const config = {
    development : [
        'babel-polyfill',
        './app'
    ],
    production  : {
        main    : [
            'babel-polyfill',
            './app'
        ],
        vendors : [
            'react',
            'react-dom',
            'react-redux',
            'redux',
            'core-js'
        ]
    }
};

module.exports = config[global.webpack.env];
