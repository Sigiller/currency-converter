import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import converterActions from './actions';
import { connect } from 'react-redux';
import Helper from '../../blocks/helper';
import CurrencyMenu from '../../blocks/currency-menu';
import { CURRENCIES_LIST } from '../../constants';
import Paper from 'material-ui/Paper';
import AmountField from '../../blocks/amount-field';
import IconButton from 'material-ui/IconButton';
import SwapIcon from 'material-ui/svg-icons/action/swap-horiz';
import style from './style.pcss';

const classnames = classNames.bind(style);

class ConverterPage extends Component {

    static displayName = '[page] Converter Page';

    //TODO refactor base/rate props

    static propTypes = {
        currencies         : PropTypes.object,
        baseCurrency       : PropTypes.string,
        baseAmount         : PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        rateCurrency       : PropTypes.string,
        rateAmount         : PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        baseCurrencySymbol : PropTypes.string,
        rateCurrencySymbol : PropTypes.string,
        getCurrencies      : PropTypes.func,
        setSelectedCurrency: PropTypes.func,
        setCurrencyAmount  : PropTypes.func,
        swapCurrencies     : PropTypes.func,
        isFetching         : PropTypes.bool,
        defaultRateCurrency: PropTypes.string,
        defaultBaseCurrency: PropTypes.string,
        setDefaultCurrency : PropTypes.func
    };

    componentDidMount() {
        Helper.validateCache();

        this.getCurrencies(this.props.baseCurrency, this.props.rateCurrency);
    }

    getCurrencies(base, symbols) {
        this.props.getCurrencies({
            base,
            symbols
        });
    }

    handleBaseCurrencyChange = (event, index, value) => {
        this.getCurrencies(value, this.props.rateCurrency);
        this.props.setSelectedCurrency({
            baseCurrency      : value,
            baseCurrencySymbol: CURRENCIES_LIST[value].symbol
        });
    }

    handleRateCurrencyChange = (event, index, value) => {
        this.getCurrencies(this.props.baseCurrency, value);
        this.props.setSelectedCurrency({
            rateCurrency      : value,
            rateCurrencySymbol: CURRENCIES_LIST[value].symbol
        });
    }

    handleAmount = (event, newVal) => {
        this.props.setCurrencyAmount({
            type : event.target.name,
            value: parseFloat(newVal, 10)
        });
    }

    swapCurrencies = () => {
        this.props.swapCurrencies();
    }

    render() {
        const amountProps = {
            onChange : this.handleAmount,
            type     : 'number',
            disabled : this.props.isFetching,
            fullWidth: true
        }

        return (
            <div className={classnames('converter')}>
                <Paper zDepth={2} className={classnames('converter-content')}>
                    <div className={classnames('converter-content__block')}>
                        <CurrencyMenu
                            value={this.props.baseCurrency}
                            onChange={this.handleBaseCurrencyChange}
                            otherValue={this.props.rateCurrency}
                            otherDefault={this.props.defaultRateCurrency}
                            defaultCurrency={this.props.defaultBaseCurrency}
                            floatingLabelText="Select base currency"
                            name="BASE"
                            setDefaultCurrency={this.props.setDefaultCurrency}
                            fullWidth={true}
                        />
                        <AmountField
                            value={this.props.baseAmount}
                            name="baseAmount"
                            currencySymbol={this.props.baseCurrencySymbol}
                            {...amountProps}
                        />
                    </div>
                    <IconButton
                        className={classnames('converter-content__swap')}
                        tooltip="Swap"
                        onClick={this.swapCurrencies}
                        children={<SwapIcon />}
                    />
                    <div className={classnames('converter-content__block')}>
                        <CurrencyMenu
                            value={this.props.rateCurrency}
                            otherValue={this.props.baseCurrency}
                            otherDefault={this.props.defaultBaseCurrency}
                            onChange={this.handleRateCurrencyChange}
                            defaultCurrency={this.props.defaultRateCurrency}
                            floatingLabelText="Select currency"
                            name="RATE"
                            setDefaultCurrency={this.props.setDefaultCurrency}
                            fullWidth={true}
                        />
                        <AmountField
                            value={this.props.rateAmount}
                            name="rateAmount"
                            currencySymbol={this.props.rateCurrencySymbol}
                            {...amountProps}
                        />
                    </div>

                </Paper>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({ ...state.converter });

const mapDispatchToProps = (dispatch) => {
    return {
        getCurrencies      : (params) => dispatch(converterActions.getCurrencies(params)),
        setSelectedCurrency: (value) => dispatch(converterActions.setSelectedCurrency(value)),
        setCurrencyAmount  : (value) => dispatch(converterActions.setCurrencyAmount(value)),
        swapCurrencies     : () => dispatch(converterActions.swapCurrencies()),
        setDefaultCurrency : (value) => dispatch(converterActions.setDefaultCurrency(value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConverterPage);
