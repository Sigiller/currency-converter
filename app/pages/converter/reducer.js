import { handleActions } from 'redux-actions-helper';
import actions from './actions';
import { CURRENCIES_LIST } from '../../constants';

const defaultBase = localStorage.getItem('DEFAULT_BASE') || 'USD';
const defaultRate = localStorage.getItem('DEFAULT_RATE') || 'RUB';

const initialState = {
    error              : null,
    isFetching         : false,
    currencyRate       : 0,
    baseCurrency       : defaultBase,
    baseAmount         : 1,
    rateCurrency       : defaultRate,
    rateAmount         : 1,
    rateCurrencySymbol : CURRENCIES_LIST[defaultRate].symbol,
    baseCurrencySymbol : CURRENCIES_LIST[defaultBase].symbol,
    defaultBaseCurrency: defaultBase,
    defaultRateCurrency: defaultRate
};

const reducer = handleActions({
    [actions.getCurrencies]: (state) => {
        return {
            ...state,
            error     : null,
            isFetching: true
        }
    },
    [actions.getCurrencies.success]: (state, action) => {
        const currencyRate = action.payload.rates[state.rateCurrency];

        return {
            ...state,
            currencyRate,
            rateAmount: (currencyRate * parseFloat(state.baseAmount, 10)).toFixed(2),
            isFetching: false
        }
    },
    [actions.getCurrencies.fail]: (state, action) => {
        return {
            ...state,
            isFetching: false,
            error     : action.payload
        };
    },
    [actions.setCurrencyAmount]: (state, action) => {
        const { type, value } = action.payload;
        let { baseAmount, rateAmount } = state;

        if(type === 'baseAmount') {
            baseAmount = value;
            rateAmount = (state.currencyRate * value).toFixed(2);
        } else {
            rateAmount = value;
            baseAmount = (value / state.currencyRate).toFixed(2);
        }

        return {
            ...state,
            baseAmount,
            rateAmount
        }
    },
    [actions.setSelectedCurrency]: (state, action) => {
        return {
            ...state,
            ...action.payload
        }
    },
    [actions.swapCurrencies]: (state) => {
        return {
            ...state,
            baseCurrency      : state.rateCurrency,
            baseAmount        : state.rateAmount,
            rateCurrency      : state.baseCurrency,
            rateAmount        : state.baseAmount,
            rateCurrencySymbol: state.baseCurrencySymbol,
            baseCurrencySymbol: state.rateCurrencySymbol,
            currencyRate      : 1 / state.currencyRate
        }
    },
    [actions.setDefaultCurrency]: (state, action) => {
        localStorage.setItem(`DEFAULT_${action.payload.type}`, action.payload.value);
        let type = '';

        action.payload.type === 'BASE' ? type = 'defaultBaseCurrency' : type = 'defaultRateCurrency';

        return {
            ...state,
            [type]: action.payload.value
        }
    }
}, initialState);

export default reducer;
