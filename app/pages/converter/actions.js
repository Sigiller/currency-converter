import { createAction } from 'redux-actions-helper';
import Helper from '../../blocks/helper';

export const getCurrencies = createAction(
    'GET_CURRENCIES', (params) => {
        return Helper.fetchCurrencies(params);
    }
);

export const setSelectedCurrency = createAction('SET_SELECTED_CURRENCY', (value) => value)

export const setCurrencyAmount = createAction('SET_CURRENCY_AMOUNT', (value) => value)

export const swapCurrencies = createAction('SWAP_CURRENCIES')

export const setDefaultCurrency = createAction('SET_DEFAULT_CURRENCY', (value) => value)

export default {
    getCurrencies,
    setSelectedCurrency,
    setCurrencyAmount,
    swapCurrencies,
    setDefaultCurrency
};
