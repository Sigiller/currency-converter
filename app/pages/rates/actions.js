import { createAction } from 'redux-actions-helper';
import Helper from '../../blocks/helper';

export const getCurrenciesRates = createAction(
    'GET_CURRENCIES_RATES', (params) => {
        return Helper.fetchCurrencies(params);
    }
);

export const setSelectedCurrency = createAction('SET_CURRENCY_RATES', (value) => value)

export const setDefaultRates = createAction('SET_DEFAULT_RATES', (value) => value)

export default {
    getCurrenciesRates,
    setSelectedCurrency,
    setDefaultRates
};
