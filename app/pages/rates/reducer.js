import { handleActions } from 'redux-actions-helper';
import actions from './actions';

const defaultCurrency = localStorage.getItem('DEFAULT_RATES_CURRENCY') || 'RUB';

const initialState = {
    error        : null,
    isFetching   : false,
    currencyRates: {},
    ratesBase    : defaultCurrency,
    defaultCurrency
};

const reducer = handleActions({
    [actions.getCurrenciesRates]: (state) => {
        return {
            ...state,
            error     : null,
            isFetching: true
        }
    },
    [actions.getCurrenciesRates.success]: (state, action) => {
        return {
            ...state,
            currencyRates: action.payload.rates,
            isFetching   : false
        }
    },
    [actions.getCurrenciesRates.fail]: (state, action) => {
        return {
            ...state,
            isFetching: false,
            error     : action.payload
        };
    },
    [actions.setSelectedCurrency]: (state, action) => {
        return {
            ...state,
            ...action.payload
        }
    },
    [actions.setDefaultCurrency]: (state, action) => {
        localStorage.setItem('DEFAULT_RATES_CURRENCY', action.payload.value);

        return {
            ...state,
            defaultCurrency: action.payload.value
        }
    }
}, initialState);

export default reducer;
