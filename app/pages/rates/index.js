import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import ratesActions from './actions';
import { connect } from 'react-redux';
import isEmpty from 'lodash.isempty';
import { Table, TableBody, TableRow, TableRowColumn } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import CurrencyMenu from '../../blocks/currency-menu';
import Helper from '../../blocks/helper';
import style from './style.pcss';

const classnames = classNames.bind(style);

class RatesPage extends Component {

    static displayName = '[page] Rates Page';

    static propTypes = {
        getCurrenciesRates : PropTypes.func.isRequired,
        setSelectedCurrency: PropTypes.func.isRequired,
        setDefaultRates    : PropTypes.func.isRequired,
        ratesBase          : PropTypes.string,
        currencyRates      : PropTypes.object,
        defaultCurrency    : PropTypes.string
    }

    componentWillMount() {
        this.props.getCurrenciesRates({ base: this.props.ratesBase });
    }

    setDefaultRates() {
        const { setDefaultRates, ratesBase } = this.props;

        setDefaultRates(ratesBase);
    }

    handleBaseChange = (event, index, value) => {
        this.props.getCurrenciesRates({ base: value });
        this.props.setSelectedCurrency({ ratesBase: value });
    }

    get elTable() {
        return !isEmpty(this.props.currencyRates) ? (
            <Table className={classnames('rates-page__table')}>
                <TableBody
                    displayRowCheckbox={true}
                    stripedRows={true}
                    children={this.elTableRows}
                />
            </Table>
        ) : null; //TODO отображение ошибки загрузки данных
    }

    get elTableRows() {
        const { currencyRates, ratesBase } = this.props;

        return Object.keys(currencyRates).map((currency, index) => (
            <TableRow key={index}>
                <TableRowColumn>{index}</TableRowColumn>
                <TableRowColumn>{this.getFlagSrc(currency)} {currency}</TableRowColumn>
                <TableRowColumn>{`${Helper.getCurrencySymbol(ratesBase, 1 / currencyRates[currency])}`}</TableRowColumn>
            </TableRow>
        ))
    }

    get elBaseSelect() {
        const props = {
            setDefaultCurrency: this.setDefaultRates,
            options           : this.options,
            fullWidth         : false,
            value             : this.props.ratesBase,
            onChange          : this.handleBaseChange,
            defaultCurrency   : this.props.defaultCurrency,
            floatingLabelText : 'Select base currency',
            name              : 'BASE'
        }

        return <CurrencyMenu {...props} />
    }

    getFlagSrc = (currency) => (
        <img
            className={classnames('rates-table__flag')}
            src={`http://www.countryflags.io/${currency.substr(0, 2)}/flat/32.png`}
            alt={`${currency} flag`}
        />
    )

    render() {
        return (
            <div className={classnames('rates')}>
                <Paper zDepth={2} className={classnames('rates-content')}>
                    {this.elBaseSelect}
                    {this.elTable}
                </Paper>
            </div>
        )
    }

}

const mapStateToProps = (state) => ({ ...state.rates });

const mapDispatchToProps = (dispatch) => {
    return {
        getCurrenciesRates : (params) => dispatch(ratesActions.getCurrenciesRates(params)),
        setSelectedCurrency: (value) => dispatch(ratesActions.setSelectedCurrency(value)),
        setDefaultRates    : (value) => dispatch(ratesActions.setDefaultRates(value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RatesPage);
