import { combineReducers } from 'redux';
import converter from './pages/converter/reducer';
import rates from './pages/rates/reducer';
import nav from './blocks/nav/reducer';

export default combineReducers({
    converter,
    rates,
    nav
});
