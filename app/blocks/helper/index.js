import axios from 'axios';
import moment from 'moment';
import { CURRENCIES_LIST } from '../../constants';
import get from 'lodash.get';

const API = 'https://api.fixer.io/latest';
const STORAGE_PREFIX = 'CUR';

// Данные обновляются раз в сутки по будним дням в 4PM CET (UTC+1), 10 минут заложил на случай если это происходит не мгновенно
const API_UPDATE_TIME = 15;
const API_UPDATE_LAG = 10;

function getDateData() {
    let curDateStr = '';
    let prevDateStr = '';

    switch(moment().day()) {
        case 0:
        case 1:
            curDateStr = moment().subtract(6, 'days')
                .day(5)
                .format('YYYY-MM-DD');
            prevDateStr = curDateStr;
            break;
        default:
            prevDateStr = moment().subtract(1, 'days')
                .format('YYYY-MM-DD');
            if(new Date().getUTCHours < API_UPDATE_TIME) {
                curDateStr = prevDateStr;
            } else {
                curDateStr = moment().format('YYYY-MM-DD');
            }
    }

    return {
        curDateStr,
        prevDateStr
    }
}

const TODAY = getDateData().curDateStr;

function getNextWorkDay() {
    switch(moment().day()) {
        case 5:
        case 6:
            return moment().add(6, 'days')
                .day(1)
                .format('DD');
        default:
            return moment().add(1, 'days')
                .format('DD');
    }
}

function isDateValid(dateData, cachedDate) {
    return cachedDate === dateData.curDateStr;
}

class Helper {

    fetchCurrencies(params) {
        const path = `${API}`;

        return new Promise((resolve) => {
            const cachedItem = this.checkCache(params);

            if(cachedItem) { resolve(JSON.parse(cachedItem)) }

            return axios.get(path, { params })
                .then((response) => {
                    localStorage.setItem(`${STORAGE_PREFIX}_${params.base}-${params.symbols}_${response.data.date}`, JSON.stringify(response.data));

                    resolve(response.data);
                })
                .catch((err) => {
                    let error = { ...err };

                    if(error.response) {
                        error = err.response.data.error ? err.response.data.error : err.response.data.message;
                    } else {
                        error = 'unknown error';
                    }

                    throw new Error(error);
                })
        })
    }

    checkCache(params) {
        return localStorage.getItem(`${STORAGE_PREFIX}_${params.base}-${params.symbols}_${TODAY}`);
    }

    validateCache() {
        const dateData = getDateData();
        const cache = Object.keys(localStorage).filter((item) => {
            return item.split('_')[0] === STORAGE_PREFIX;
        })

        cache.forEach((cachedItem) => {
            const cachedDate = cachedItem.split('_')[2];

            if(!isDateValid(dateData, cachedDate)) { localStorage.removeItem(cachedItem); }
        })
    }

    getNextValidationTime() {
        const currentTime = new Date();
        const validationTime = new Date();

        validationTime.setHours(API_UPDATE_TIME);
        validationTime.setMinutes(API_UPDATE_LAG);

        if(currentTime.getUTCHours >= API_UPDATE_TIME && currentTime.getMinutes > API_UPDATE_TIME) {
            validationTime.setDate(getNextWorkDay())
        }

        return validationTime.getTime() - currentTime.getTime();
    }

    getCurrencySymbol(currency, amount) {
        const localeSettings = {
            style   : 'currency',
            currency: ratesBase
        };
        const defaultValue = Number().toLocaleString('ru-RU', localeSettings).split(' ')[1];

        return `${amount ? `${amount} ` : ''}${get(CURRENCIES_LIST, `${currency}.symbol`, defaultValue)}`;
    }

}

export default new Helper();
