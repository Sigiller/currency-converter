import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import FlagIcon from 'material-ui/svg-icons/content/flag';
import { CURRENCIES_LIST } from '../../constants';
import style from './style.pcss';

const classnames = classNames.bind(style);

class CurrencyMenu extends Component {

    static displayName = '[block] Currency Select';

    static propTypes = {
        value             : PropTypes.string,
        otherValue        : PropTypes.string,
        name              : PropTypes.string,
        defaultCurrency   : PropTypes.string,
        setDefaultCurrency: PropTypes.func,
        onChange          : PropTypes.func.isRequired,
        otherDefault      : PropTypes.string
    }

    constructor() {
        super();

        this.setDefaultCurrency = this.setDefaultCurrency.bind(this);
    }

    setDefaultCurrency() {
        const { setDefaultCurrency, name, value } = this.props;

        setDefaultCurrency({
            type: name,
            value
        });
    }

    get options() {
        return Object.keys(CURRENCIES_LIST).map((currency) => {
            return {
                value: currency,
                label: CURRENCIES_LIST[currency].name
            }
        })
    }

    get elMenuItems() {
        const options = this.options.filter((item) => item.value !== this.props.otherValue)

        return options.map((option, index) => {
            const flag = this.getFlagSrc(option.value);

            return (
                <MenuItem
                    className={classnames('currency-menu__item')}
                    key={index}
                    value={option.value}
                    leftIcon={flag}
                    secondaryText={option.value}
                    primaryText={option.label}
                    label={<p className={classnames('currency-menu__label')}>{flag} {option.label}</p>}
                />
            )
        })
    }

    getFlagSrc = (currency) => (
        <img
            className={classnames('currency-menu__flag')}
            src={`http://www.countryflags.io/${currency.substr(0, 2)}/flat/32.png`}
            alt={`${currency} flag`}
        />
    )

    render() {
        const { defaultCurrency, otherDefault, value, onChange } = this.props;
        const isDefaultYet = defaultCurrency === value;
        const cantBeDefault = otherDefault === value;

        return (
            <div className={classnames('currency-menu')}>
                <SelectField
                    className={classnames('currency-menu__field')}
                    value={value}
                    children={this.elMenuItems}
                    onChange={onChange}
                />
                <IconButton
                    className={classnames({
                        'currency-menu__default'    : true,
                        'currency-menu__default-yet': isDefaultYet
                    })}
                    tooltip="Set Default"
                    disabled={isDefaultYet || cantBeDefault}
                    onClick={this.setDefaultCurrency}
                    children={<FlagIcon />}
                />
            </div>
        )
    }

}

export default CurrencyMenu;
