import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import FlatButton from 'material-ui/FlatButton';
import AppBar from 'material-ui/AppBar';
import ChartIcon from 'material-ui/svg-icons/editor/show-chart';
import SwapIcon from 'material-ui/svg-icons/action/swap-horiz';
import IconButton from 'material-ui/IconButton';
import { connect } from 'react-redux';
import navActions from './actions';
import ConverterPage from '../../pages/converter';
import RatesPage from '../../pages/rates';
import style from './style.pcss';

const classnames = classNames.bind(style);

class Nav extends PureComponent {

    static displayName = '[block] Nav';

    static propTypes = {
        toggleActivePage : PropTypes.func.isRequired,
        isConverterActive: PropTypes.bool.isRequired
    }

    get elButtons() {
        const { isConverterActive, toggleActivePage } = this.props;

        return (
            <div className={classnames('nav-buttons')}>
                <FlatButton
                    label="Converter"
                    onClick={toggleActivePage}
                    disabled={isConverterActive}
                />
                <FlatButton
                    label="Rates"
                    onClick={toggleActivePage}
                    disabled={!isConverterActive}
                />
            </div>
        )
    }

    get elTitleButton() {
        const { isConverterActive, toggleActivePage } = this.props;

        return isConverterActive ? (
            <IconButton
                onClick={toggleActivePage}
                children={<SwapIcon color="#ffffff" />}
            />
        ) : (
            <IconButton
                onClick={toggleActivePage}
                children={<ChartIcon color="#ffffff" />}
            />
        )
    }

    render() {
        const { isConverterActive, toggleActivePage } = this.props;

        return (
            <div>
                <AppBar
                    className={classnames('nav')}
                    title={isConverterActive ? 'Currency Converter' : 'Currency Rates'}
                    iconElementLeft={this.elTitleButton}
                    onTitleClick={toggleActivePage}
                    iconElementRight={this.elButtons}
                />
                {isConverterActive ? <ConverterPage /> : <RatesPage />}
            </div>
        )
    }

}

const mapStateToProps = (state) => ({ ...state.nav });

const mapDispatchToProps = (dispatch) => {
    return { toggleActivePage: () => dispatch(navActions.toggleActivePage()) }
}

export default connect(mapStateToProps, mapDispatchToProps)(Nav);
