import { handleActions } from 'redux-actions-helper';
import actions from './actions';

const initialState = {
    isConverterActive: true
};

const reducer = handleActions({
    [actions.toggleActivePage]: (state) => {
        return {
            ...state,
            isConverterActive: !state.isConverterActive
        }
    }
}, initialState);

export default reducer;
