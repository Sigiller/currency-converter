import { createAction } from 'redux-actions-helper';

const toggleActivePage = createAction('TOGGLE_ACTIVE_PAGE');

export default {
    toggleActivePage
}
