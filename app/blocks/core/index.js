import { createStore as _createStore, applyMiddleware } from 'redux';
import { promiseMiddleware } from 'redux-actions-helper';

export const createStore = function(data) {
    const middleware = [
        promiseMiddleware
    ];
    let createStoreWithMiddleware;

    if(__DEVELOPMENT__) {
        const { createLogger } = require('redux-logger');
        const logger = createLogger({
            duration : true,
            collapsed: true,
            diff     : true
        });

        middleware.push(logger);
    }

    createStoreWithMiddleware = applyMiddleware(...middleware)(_createStore);
    const reducers = require('../../reducers').default;
    const store = createStoreWithMiddleware(reducers, data);

    if(__DEVELOPMENT__ && module.hot) {
        module.hot.accept('./../../reducers', () => {
            store.replaceReducer(require('./../../reducers').default);
        });
    }

    return store;
}
