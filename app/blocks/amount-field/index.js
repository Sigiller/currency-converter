import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import TextField from 'material-ui/TextField';
import style from './style.pcss';

const classnames = classNames.bind(style);

const AmountField = (props) => {
    const { currencySymbol, ...fieldProps } = props;

    return (
        <div className={classnames('currency-amount')}>
            <span className={classnames('currency-amount__symbol')}>{currencySymbol}</span>
            <TextField className={classnames('currency-amount__field')} {...fieldProps} />
        </div>
    )
}

AmountField.displayName = '[block] Amount Field';

AmountField.propTypes = {
    currencySymbol: PropTypes.string
}

export default AmountField;
