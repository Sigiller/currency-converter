import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from './blocks/core';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { indigo500, teal500 } from 'material-ui/styles/colors';
import Nav from './blocks/nav'

const store = createStore({});

const mountNode = document.getElementById('app');

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: indigo500,
        primary2Color: teal500
    }
});

render(
    <Provider store={store} key="provider">
        <MuiThemeProvider muiTheme={muiTheme}>
            <Nav />
        </MuiThemeProvider>
    </Provider>,
    mountNode
)
