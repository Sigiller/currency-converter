/* eslint-disable */
export const CURRENCIES_LIST = {
    "USD": {
      "name": "United States Dollar",
      "symbol": "$"
    },
    "EUR": {
      "name": "Euro",
      "symbol": "€"
    },
    "GBP": {
      "name": "Pound sterling",
      "symbol": "£"
    },
    "RUB": {
      "name": "Russian ruble",
      "symbol": "₽"
    },
    "AUD": {
      "name": "Australian dollar",
      "symbol": "$"
    },
    "BGN": {
      "name": "Bulgarian lev",
      "symbol": "лв"
    },
    "BRL": {
      "name": "Brazilian real",
      "symbol": "R$"
    },
    "CAD": {
      "name": "Canadian dollar",
      "symbol": "$"
    },
    "CHF": {
      "name": "Swiss franc",
      "symbol": "CHF"
    },
    "CNY": {
      "name": "Chinese yuan",
      "symbol": "¥"
    },
    "CZK": {
      "name": "Czech koruna",
      "symbol": "Kč"
    },
    "DKK": {
      "name": "Danish krone",
      "symbol": "kr"
    },
    "HKD": {
      "name": "Hong Kong dollar",
      "symbol": "$"
    },
    "HRK": {
      "name": "Croatian kuna",
      "symbol": "kn"
    },
    "HUF": {
      "name": "Hungarian forint",
      "symbol": "Ft"
    },
    "IDR": {
      "name": "Indonesian rupiah",
      "symbol": "Rp"
    },
    "ILS": {
      "name": "Israeli shekel",
      "symbol": "₪"
    },
    "INR": {
      "name": "Indian rupee",
      "symbol": ""
    },
    "JPY": {
      "name": "Japanese yen",
      "symbol": "¥"
    },
    "KRW": {
      "name": "South Korean won",
      "symbol": "₩"
    },
    "MXN": {
      "name": "Mexican peso",
      "symbol": "$"
    },
    "MYR": {
      "name": "Malaysian ringgit",
      "symbol": "RM"
    },
    "NOK": {
      "name": "Norwegian krone",
      "symbol": "kr"
    },
    "NZD": {
      "name": "New Zealand dollar",
      "symbol": "$"
    },
    "PHP": {
      "name": "Philippine peso",
      "symbol": "₱"
    },
    "PLN": {
      "name": "Polish zloty",
      "symbol": "zł"
    },
    "RON": {
      "name": "Romanian leu",
      "symbol": "lei"
    },
    "SEK": {
      "name": "Swedish krona",
      "symbol": "kr"
    },
    "SGD": {
      "name": "Singapore dollar",
      "symbol": "$"
    },
    "THB": {
      "name": "Thai baht",
      "symbol": "฿"
    },
    "TRY": {
      "name": "Turkish lira",
      "symbol": ""
    },
    "ZAR": {
      "name": "South African rand",
      "symbol": "R"
    }
  }
